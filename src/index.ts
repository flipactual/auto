import wca from './util/wca';
import draw from './util/draw';
import getRules from './util/getRules';
import setupCanvas from './util/setupCanvas';
import generateInput from './util/generateInput';
import getInputValue from './util/getInputValue';
import { setInvocationID } from './util/invocationID';

const button = document.getElementById('button') as HTMLInputElement;

const canvas = document.getElementById('canvas') as HTMLCanvasElement;
const canvasWrapper = document.getElementById('canvas-wrapper') as HTMLElement;

const { clientWidth: width, clientHeight: height } = canvasWrapper;
canvas.width = width;
canvas.height = height;

const ctx = canvas.getContext('2d') as CanvasRenderingContext2D;

const drawWCA = (): void => {
  const i = getInputValue();
  const id = setInvocationID();

  if (i < 0 || i > 255) {
    return;
  }

  setupCanvas(ctx, width, height);

  draw(id, ctx, wca(getRules(i)), height, generateInput(width), 0)();
};

button.addEventListener('click', drawWCA);

drawWCA();
