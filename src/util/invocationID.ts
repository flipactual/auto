let id = 0;

export const setInvocationID = (): number => {
  id += 1;
  return id;
};

export const getInvocationID = (): number => {
  return id;
};
