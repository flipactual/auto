import { prepend, propEq, reverse, split, until } from 'ramda';

const toBinaryString = (n: number): string => n.toString(2);

const getRules = (n: number): Array<string> =>
  reverse(
    until(propEq('length', 8), prepend('0'))(split('', toBinaryString(n)))
  );

export default getRules;
