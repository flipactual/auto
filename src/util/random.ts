import Prando from 'prando';

const rng = new Prando('🧿');
const random = (): number => rng.next(0, 1);

export default random;
