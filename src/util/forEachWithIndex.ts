import { addIndex, forEach } from 'ramda';

const forEachWithIndex = addIndex(forEach);

export default forEachWithIndex;
