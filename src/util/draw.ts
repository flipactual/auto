import forEach from './forEachWithIndex';
import { getInvocationID } from './invocationID';

const draw = (
  id: number,
  ctx: CanvasRenderingContext2D,
  ca: (input: Array<string>) => Array<string>,
  height: number,
  input: Array<string>,
  y: number
) => (): void => {
  if (getInvocationID() === id) {
    const output: Array<string> = ca(input);

    if (y > height - 1) {
      const pixels = ctx.getImageData(0, 0, output.length, height);
      ctx.putImageData(pixels, 0, -1);
      ctx.clearRect(0, height - 1, output.length, height - 1);
      ctx.beginPath();
    }

    forEach((_, x) => {
      _ === '1' && ctx.fillRect(x, y > height - 1 ? height - 1 : y, 1, 1);
    })(output);

    requestAnimationFrame(draw(id, ctx, ca, height, output, y + 1));
  }
};

export default draw;
