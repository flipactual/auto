import { aperture, append, join, map, pipe, prepend } from 'ramda';

const parseBinary = (s: string): number => parseInt(s, 2);
const getRule = (rules: Array<string>) => (n: number): string => rules[n];

const wca = (rules: Array<string>): ((_: Array<string>) => Array<string>) =>
  pipe(
    aperture(3),
    map(
      pipe(
        join(''),
        parseBinary,
        getRule(rules)
      )
    ),
    append('0'),
    prepend('0')
  );

export default wca;
