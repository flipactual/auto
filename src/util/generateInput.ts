import random from './random';

const generateInput = (width: number): Array<string> =>
  [...Array(Math.floor(width))].map(() => (random() > 0.5 ? '1' : '0'));

export default generateInput;
