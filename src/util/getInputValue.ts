const getInputValue = (): number => {
  const input = document.getElementById('input') as HTMLInputElement;
  return +input.value;
};

export default getInputValue;
